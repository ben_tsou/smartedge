FROM maven:3.3.3-jdk-8

RUN mkdir MessageSigner
WORKDIR MessageSigner
ADD . .
ADD ./runtime-artifacts .

RUN ["mvn","clean","verify", "package"]

ENTRYPOINT ["java", "-jar", "./target/App.jar"]
