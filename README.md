# Readme

### Generate RSA keypair

Generate RSA private key
```
openssl genrsa -out private.pem
```

Convert to DER
```
openssl pkcs8 -topk8 -inform PEM -outform DER -in private.pem -out private.der -nocrypt
```

Generate the corresponding public key
```
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

### Running the App
On the project root directory:
```
# To run tests
> mvn test

# Generate executable Jar
> mvn package

# Run the executable Jar
> java -jar ./target/App.jar some-message
```

### CI
Generally CI would follow these stages:
1. Check out the source from version control.
2. Run unit tests (`mvn verify`)
3. Build executable Jar (`mvn package`)
4. Containerize and distribute