package com.bentsou;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.log4j.Log4j2;
import org.junit.After;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@Log4j2
public class SerializerTest {
    private ObjectMapper mockObjectMapper = mock(ObjectMapper.class);
    private ObjectWriter mockObjectWriter = mock(ObjectWriter.class);

    @After
    public void tearDown() {
        reset(mockObjectMapper, mockObjectWriter);
    }

    @Test
    public void serialize_shouldCallObjectMapper() {
        givenStandardObjectMapper();

        Serializer serializer = new Serializer(mockObjectMapper);
        serializer.serialize(new Object());

        verify(mockObjectMapper, times(1)).writerWithDefaultPrettyPrinter();
    }

    @Test(expected = RuntimeException.class)
    public void serialize_shouldThrowRuntimeException_whenObjectMapperErrors() {
        givenObjectMapperThrowingException();

        Serializer serializer = new Serializer(mockObjectMapper);
        serializer.serialize(new Object());
    }

    private void givenStandardObjectMapper() {
        when(mockObjectMapper.writerWithDefaultPrettyPrinter()).thenReturn(mockObjectWriter);
    }

    private void givenObjectMapperThrowingException() {
        when(mockObjectMapper.writerWithDefaultPrettyPrinter()).thenReturn(mockObjectWriter);

        try {
            when(mockObjectWriter.writeValueAsString(any())).thenThrow(JsonProcessingException.class);
        } catch (JsonProcessingException e) {
            log.error("Should not reach this point", e);
        }

    }
}
