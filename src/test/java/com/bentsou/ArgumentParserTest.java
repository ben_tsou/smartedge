package com.bentsou;

import org.junit.Assert;
import org.junit.Test;

public class ArgumentParserTest {
    private ArgumentParser argumentParser = new ArgumentParser();

    @Test(expected = IllegalArgumentException.class)
    public void getMessageArgument_shouldError_whenArgumentIsEmpty() {
        String[] args = new String[] {};
        argumentParser.getMessageArgument(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getMessageArgument_shouldError_whenMoreThanOneArgument() {
        String[] args = new String[] {"message1", "message2"};
        argumentParser.getMessageArgument(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getMessageArgument_shouldError_whenOnlyArgumentLongerThan250() {
        StringBuilder messageBuilder = new StringBuilder();
        for (int i = 0; i <= 250; i++) {
            messageBuilder.append("x");
        }

        String[] args = new String[] {messageBuilder.toString()};
        argumentParser.getMessageArgument(args);
    }

    @Test
    public void getMessageArgument_shouldReturnMessage_whenMessageGiven() {
        String expectedMessage = "message1";
        String[] args = new String[] {expectedMessage};

        String message = argumentParser.getMessageArgument(args);

        Assert.assertEquals(expectedMessage, message);
    }
}
