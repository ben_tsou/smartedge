package com.bentsou;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class MessageSignerTest {
    private Path publicKeyPath;
    private Path invalidPublicKeyPath;
    private Path privateKeyPath;
    private Path invalidPrivateKeyPath;

    @Before
    public void setup() {
        publicKeyPath = Paths.get(".","src/test/resources/test-public.pem");
        invalidPublicKeyPath = Paths.get(".","src/test/resources/nonexistent-public.pem");
        privateKeyPath = Paths.get(".", "src/test/resources/test-private.der");
        invalidPrivateKeyPath = Paths.get(".", "src/test/resources/nonexistent-private.der");
    }

    @Test(expected = RuntimeException.class)
    public void constructor_shouldThrowException_withInvalidPublicKeyPath() {
        new MessageSigner(invalidPrivateKeyPath, invalidPublicKeyPath);
    }

    @Test
    public void sign_shouldReturnSignature() {
        String expectedSignature = "EEfj9baaZhHDVWj8J4aUfA2yXz/estJdyNLVyDW6NBo+uuFH7W8wskx773lWcJ+jbA4ADYx2HfeVBi/5sYYOiMsnHl8IMIskXL71MBQ3lMJG3cvd1NjM0K+TOlKMvOKIsLSIezsw20zx+LYF+nMjinpFzFT3IIn55Qen0IFVlrgdwPij8t3jewXecRRrGsvc9dFthN1RrDDfCEeOA8SlN+4v5gjlKaPb3c8CaYD5cVpdiHk29l4amABOQFDrq9cA4htI5xKXD+TwDHmkQmtwuY7JtlZVhx7/IEF3DoFVW3IGD3XgLPHLcA931MbVAHP4sm8FRCD68znNX754+H/H7w==";

        MessageSigner signer = new MessageSigner(privateKeyPath, publicKeyPath);

        String actualSignature = signer.sign("test-message");
        Assert.assertEquals(expectedSignature, actualSignature);
    }

    @Test(expected = RuntimeException.class)
    public void sign_shouldThrowException_withInvalidPrivateKeyPath() {
        MessageSigner signer = new MessageSigner(invalidPrivateKeyPath, publicKeyPath);
        signer.sign("test-message");
    }

    @Test(expected = RuntimeException.class)
    public void sign_shouldThrowException_withInvalidPrivateKey() {
        MessageSigner signer = new MessageSigner(publicKeyPath, publicKeyPath);
        signer.sign("test-message");
    }

    @Test
    public void getPublicKey_shouldReturnPublicKey() {
        String expectedPublicKey =
                "-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnHMtGszzZa36IAD39BP/\n" +
                "moTFG5CAY4E1ZGGJm7ngqRZwd5Waa6GbOoM7Mndd6/zYfHJIeiPAK8xo3QG/2Plf\n" +
                "Hwdy342iDCKJreXrbiDdtGLjrXw/JFW0xtWuYmlmv4B7jJYeXYxnGZ+4m2wMXu6l\n" +
                "yTzJFgCxWUXqATH12+kuECVH36fV9S692m2Jnp754eAVQAdvVeGCkaOqH9GY+0y4\n" +
                "obuWj8CjWqofPhSzVfpTx+kcpnWgzcXTnbD+la1V8djTgkF728vu7qxYmN7/x+8X\n" +
                "vOIvVQhalu5yg64OFhFxq16dCpj5MXg6RkxIofycKY9GvJnPJUhrRehDP6ZI21e2\n" +
                "zwIDAQAB\n" +
                "-----END PUBLIC KEY-----";

        MessageSigner signer = new MessageSigner(privateKeyPath, publicKeyPath);

        String actualPublicKey = signer.getPublicKey();
        Assert.assertEquals(expectedPublicKey, actualPublicKey);
    }


}
