package com.bentsou;

import lombok.NonNull;


public class ArgumentParser {
    /**
     * Retrieve the "message" argument from an array of arguments.
     * @param args argument list in the form of String array.
     * @return The "message" argument
     */
    public String getMessageArgument(@NonNull String[] args) {
        validateArguments(args);
        return args[0];
    }

    private void validateArguments(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("There should be one and only one argument");
        }

        String argument = args[0];
        if (argument.length() > 250) {
            throw new IllegalArgumentException("Message argument should have less than 250 characters");
        }
    }
}
