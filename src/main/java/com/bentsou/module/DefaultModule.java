package com.bentsou.module;

import com.bentsou.ArgumentParser;
import com.bentsou.MessageSigner;
import com.bentsou.Serializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The default Guice module.
 */
public class DefaultModule extends AbstractModule {
    @Override
    protected void configure() { }

    /**
     * Provides a MessageSigner.
     *
     * @param privateKeyPath path to the private key
     * @param publicKeyPath path to the public key
     * @return a MessageSigner
     */
    @Provides
    public MessageSigner messageSigner(@Named("privateKey") Path privateKeyPath,
                                       @Named("publicKey") Path publicKeyPath) {
        return new MessageSigner(privateKeyPath, publicKeyPath);
    }

    /**
     * Provides a ArgumentParser.
     *
     * @return a ArgumentParser
     */
    @Provides
    @Singleton
    public ArgumentParser argumentParser() {
        return new ArgumentParser();
    }

    /**
     * Providers a JSON Serializer.
     *
     * @param objectMapper the Jackson ObjectMapper to use
     * @return a JSON Serializer
     */
    @Provides
    @Singleton
    public Serializer serializer(ObjectMapper objectMapper) {
        return new Serializer(objectMapper);
    }

    /**
     * Provides the private key Path.
     *
     * @return the private key Path
     */
    @Provides
    @Named("privateKey")
    public Path privateKeyPath() {
        return Paths.get("private.der");
    }

    /**
     * Provides the public key Path.
     *
     * @return the public key Path
     */
    @Provides
    @Named("publicKey")
    public Path publicKeyPath() {
        return Paths.get("public.pem");
    }

    /**
     * Provides a Jackson ObjectMapper.
     *
     * @return a Jackson ObjectMapper
     */
    @Provides
    @Singleton
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
