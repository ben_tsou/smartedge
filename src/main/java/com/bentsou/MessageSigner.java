package com.bentsou;

import com.google.common.io.BaseEncoding;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.stream.Collectors;

@Log4j2
public class MessageSigner {
    private final Path privateKeyPath;
    private String publicKeyText;

    /**
     * Constructor.
     *
     * @param privateKeyPath path to a RSA private key in PKCS8 DER format. The Path is
     *                       saved as an instance variable. The actual key is loaded when
     *                       signing
     * @param publicKeyPath path to a RSA public key. The key is loaded eagerly and kept
     *                      as an instance variable.
     */
    public MessageSigner(Path privateKeyPath, Path publicKeyPath) {
        this.privateKeyPath = privateKeyPath;

        try {
            publicKeyText = Files.lines(publicKeyPath).collect(Collectors.joining("\n"));
        } catch (IOException | RuntimeException e) {
            log.error("Unable to load public key file", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Signs a message using the private key given to this MessageSigner.
     *
     * @param message the message to sign
     * @return Base64 encoded SHA256 hashed RSA signature.
     */
    public String sign(@NonNull String message) {
        // Load the private key
        PrivateKey privateKey = null;
        try {
            byte[] privateKeyContent = Files.readAllBytes(privateKeyPath);
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyContent);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            privateKey = keyFactory.generatePrivate(privateKeySpec);
        } catch (IOException e) {
            log.error("Unable to load private key", e);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            log.error("Unable to create private key", e);
        }

        // Unable to load the private key - throw exception
        if (privateKey == null) {
            throw new RuntimeException();
        }

        // Sign
        try {
            Signature sign = Signature.getInstance("SHA256withRSA");
            sign.initSign(privateKey);
            sign.update(message.getBytes(StandardCharsets.UTF_8));
            byte[] signature = sign.sign();
            return BaseEncoding.base64().encode(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            log.error("Unable to create signature", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the Base64 encoded public key as a single line. Line breaks are encoded as "\n".
     *
     * @return Base64 encoded public key. Line breaks are encoded as "\n"
     */
    public String getPublicKey() {
        return publicKeyText;
    }
}
