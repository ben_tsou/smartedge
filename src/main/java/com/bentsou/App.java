package com.bentsou;

import com.bentsou.entity.Response;
import com.bentsou.module.DefaultModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class App {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new DefaultModule());

        ArgumentParser argumentParser = injector.getInstance(ArgumentParser.class);
        MessageSigner messageSigner = injector.getInstance(MessageSigner.class);
        Serializer serializer = injector.getInstance(Serializer.class);

        String message = argumentParser.getMessageArgument(args);
        String signature = messageSigner.sign(message);
        String publicKey = messageSigner.getPublicKey();

        Response response = Response.builder()
                                    .message(message)
                                    .pubkey(publicKey)
                                    .signature(signature)
                                    .build();

        System.out.println(serializer.serialize(response));
    }
}
