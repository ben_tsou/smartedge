package com.bentsou.entity;

import lombok.Builder;
import lombok.Data;

/**
 * Data class representing the response from this application.
 */
@Data
@Builder
public class Response {
    private final String message;
    private final String signature;
    private final String pubkey;

    public Response(String message, String signature, String pubkey) {
        this.message = message;
        this.signature = signature;
        this.pubkey = pubkey;
    }
}
